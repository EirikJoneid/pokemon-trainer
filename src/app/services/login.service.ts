import { Injectable } from '@angular/core';
import { LocalStorageUtil } from 'src/app/utils/local-storage.util';
import { TRAINER_NAME } from './../consts/localStorage';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  public login(name: string): void {
    LocalStorageUtil.setItem(TRAINER_NAME, name);
  }
}
