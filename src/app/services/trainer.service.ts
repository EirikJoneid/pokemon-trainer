import { TRAINER_COLLECTION_KEY, TRAINER_NAME } from './../consts/localStorage';
import { LocalStorageUtil } from 'src/app/utils/local-storage.util';
import { Pokemon } from './../model/pokemon.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _pokeCollection: Pokemon[] = [];

  constructor() {
    this._pokeCollection =
      LocalStorageUtil.getItem(TRAINER_COLLECTION_KEY) || [];
  }

  public addToCollection(pokemon: Pokemon): void {
    this._pokeCollection.push(pokemon);
    LocalStorageUtil.setItem<Pokemon[]>(
      TRAINER_COLLECTION_KEY,
      this._pokeCollection
    );
  }

  public removeFromCollection(pokemon: Pokemon): void {
    this._pokeCollection = this._pokeCollection.filter(
      (item) => item !== pokemon
    );

    LocalStorageUtil.setItem<Pokemon[]>(
      TRAINER_COLLECTION_KEY,
      this._pokeCollection
    );
  }

  get collection(): Pokemon[] {
    return this._pokeCollection;
  }

  public getTrainerName(): string {
    return LocalStorageUtil.getItem(TRAINER_NAME) || '';
  }
}
