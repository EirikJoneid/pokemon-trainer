import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pokemon } from '../model/pokemon.model';
import { ApiService } from './api.service';
import { PokemonResponse } from './../model/pokemon-response.model';

@Injectable({
  providedIn: 'root',
})
export class CatalogueService {
  private _catalogue: Pokemon[] = [];
  private _initialized: boolean = false;

  constructor(
    private readonly http: HttpClient,
    private readonly apiService: ApiService
  ) {}

  public fetchCatalogue(page: number): void {
    this._initialized = true;
    const url: string = this.apiService.generateUrl(page);
    const request$ = this.http.get<PokemonResponse>(url);

    request$.subscribe((res: PokemonResponse) => {
      const pokemon: Pokemon[] = res.results.map(this.addIDAndImage);
      this._catalogue = pokemon;
    });
  }

  private addIDAndImage(pokemon: Pokemon): Pokemon {
    const id = Number(pokemon.url.split('/')[6]);

    return {
      ...pokemon,
      id,
      image: `assets/pokemonImages/${id}.png`,
    };
  }

  public catalogue(): Pokemon[] {
    return this._catalogue;
  }

  public initialized(): boolean {
    return this._initialized;
  }
}
