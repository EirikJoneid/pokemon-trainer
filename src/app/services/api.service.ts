import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public generateUrl(page: number): string {
    return `https://pokeapi.co/api/v2/pokemon?offset=${
      (page - 1) * 30
    }&limit=30`;
  }
}
