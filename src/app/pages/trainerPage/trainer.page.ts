import { AppRoutes } from './../../consts/appRoutes';
import { Router } from '@angular/router';
import { TrainerService } from './../../services/trainer.service';
import { Component, OnInit } from '@angular/core';
import { Pokemon } from './../../model/pokemon.model';

@Component({
  selector: 'app-trainer.page',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage implements OnInit {
  private name: string = '';

  constructor(
    private readonly trainerService: TrainerService,
    private readonly router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.name = this.trainerService.getTrainerName();
    if (!this.name) {
      await this.router.navigate([AppRoutes.LoginPage]);
    }
  }

  get collection(): Pokemon[] {
    return this.trainerService.collection;
  }

  get collectionNames(): string[] {
    return this.trainerService.collection.map((pokemon) => pokemon.name);
  }

  get trainerName(): string {
    return this.name;
  }
}
