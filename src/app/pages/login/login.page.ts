import { AppRoutes } from './../../consts/appRoutes';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
})
export class LoginPage {
  constructor(private readonly router: Router) {}
  public async handleLoginSuccess(): Promise<void> {
    await this.router.navigate([ AppRoutes.TrainerPage ]);
  }
}
