import { Component, OnInit } from '@angular/core';
import { CatalogueService } from './../../services/catalogue.service';
import { Pokemon } from 'src/app/model/pokemon.model';
import { TrainerService } from './../../services/trainer.service';
import { Router } from '@angular/router';
import { AppRoutes } from 'src/app/consts/appRoutes';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css'],
})
export class CataloguePage implements OnInit {
  private page: number = 1;

  constructor(
    private readonly router: Router,
    private readonly catalogueService: CatalogueService,
    private readonly trainerService: TrainerService
  ) {}

  ngOnInit(): void {
    if (!this.trainerService.getTrainerName()) {
      this.router.navigate([AppRoutes.LoginPage]);
    }
    if (!this.catalogueService.initialized()) {
      this.catalogueService.fetchCatalogue(this.page);
    }
  }

  get catalogue(): Pokemon[] {
    return this.catalogueService.catalogue();
  }

  get getPage(): number {
    return this.page;
  }

  get collectionNames(): string[] {
    return this.trainerService.collection.map((pokemon) => pokemon.name);
  }

  public onNextClick(): void {
    this.page++;
    this.catalogueService.fetchCatalogue(this.page);
  }

  public onPreviousClick(): void {
    this.page--;
    this.catalogueService.fetchCatalogue(this.page);
  }
}
