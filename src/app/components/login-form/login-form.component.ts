import { Component, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from './../../services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
})
export class LoginFormComponent {
  @Output() success: EventEmitter<void> = new EventEmitter<void>();

  public name: string = "";

  constructor(private readonly loginService: LoginService) {}

  public submitLogin(loginForm: NgForm): void {
    const { name } = loginForm.value;
    this.loginService.login(name);
    this.success.emit();
  }
}
