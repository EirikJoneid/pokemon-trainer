import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/model/pokemon.model';
import { TrainerService } from './../../services/trainer.service';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css'],
})
export class PokemonCardComponent implements OnInit {
  private selected: boolean = false;
  @Input() pokemon: Pokemon | undefined;
  @Input() collectionNames: string[] = [];

  constructor(private readonly trainerService: TrainerService) {}

  ngOnInit() {
    if (this.pokemon) {
      const poke = this.pokemon;

      if (this.collectionNames.includes(poke.name)) {
        this.selected = true;
      }
    }
  }

  @HostListener('click') onClick() {
    if (this.pokemon) {
      this.selected = !this.selected;
      if (this.selected) {
        this.trainerService.addToCollection(this.pokemon);
      } else {
        this.trainerService.removeFromCollection(this.pokemon);
      }
    }
  }

  get style(): string {
    if (this.selected) {
      return 'pokemonCard pokemonCardClicked';
    }
    return 'pokemonCard';
  }
}
