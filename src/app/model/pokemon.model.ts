export interface Pokemon {
  name: string;
  url: string;
  id?: number;
  gif?: string;
  image?: string;
}
