import { AppRoutes } from './consts/appRoutes';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainerPage/trainer.page';
import { CataloguePage } from './pages/cataloguePage/catalogue.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `/${AppRoutes.LoginPage}`,
  },
  {
    path: AppRoutes.LoginPage,
    component: LoginPage,
  },
  {
    path: AppRoutes.TrainerPage,
    component: TrainerPage,
  },
  {
    path: AppRoutes.CataloguePage,
    component: CataloguePage,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
