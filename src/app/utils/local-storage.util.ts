export class LocalStorageUtil {
  public static setItem<T>(key: string, value: T): void {
    try {
      const json: string = JSON.stringify(value);
      const encoded: string = btoa(encodeURIComponent(json));
      localStorage.setItem(key, encoded);
    } catch (err: any) {
      throw new Error(err.message);
    }
  }

  public static getItem<T>(key: string): T | null {
    const saved: string | null = localStorage.getItem(key);

    if (!saved) {
      return null;
    }

    try {
      const key: string = decodeURIComponent(atob(saved));
      return JSON.parse(key) as T;
    } catch {
      return null;
    }
  }
}
